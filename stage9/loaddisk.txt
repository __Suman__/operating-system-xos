load --os $HOME/myxos/stage9/os_startup.xsm
load --int=7 $HOME/myxos/stage9/int7.xsm
load --int=1 $HOME/myxos/stage9/int1.xsm
load --int=2 $HOME/myxos/stage9/int2.xsm
load --int=3 $HOME/myxos/stage9/int3.xsm
load --int=4 $HOME/myxos/stage9/int4.xsm
load --int=5 $HOME/myxos/stage9/int5.xsm
load --int=6 $HOME/myxos/stage9/int6.xsm
load --exhandler $HOME/myxos/stage9/exhandler.xsm
load --int=timer $HOME/myxos/stage9/timer.xsm


load --init $HOME/myxos/stage9/forktest.xsm
load --exec $HOME/myxos/stage9/exectest.xsm



load --init $HOME/myxos/stage9/odd.xsm
load --exec  $HOME/myxos/stage9/even.xsm