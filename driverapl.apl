decl
	integer status;
	integer fileid;
	integer seekstatus;
	integer writeStatus;
	string word;
	string name;
	integer numwords;
	integer variable;
enddecl
integer main()
{
	//this block for testing write without creating the file uncomment this to test this	
	//word = "S";
	//status = Write(1,word);

	//this block to test read without creating file
	//status = Read(2,word);
	//print(word);

	//this block is to check seek without creating file in xfs-interface
	//numwords = 7;
	//status = Seek(2,numwords);

	//this block is to create a file for further testing
	while(1) do
	variable=read();
	if (variable == 1) then
	read(name);
	status = Create(name);
	print(status);
	breakpoint;
	endif;
	//this block is to open the file
	if (variable == 2) then
	read(name );
	fileid = Open(name);
	print(fileid);
	breakpoint;
	endif;
	//this block is to Change the seek position to the end of the file here the end of file is at 0 as the file is just created and nothing is written to the file
	//numwords = 0;
	//seekstatus = Seek(fileid,numwords);
	//print(seekstatus);

	//this block to change the file size to a location greater than the eof we should get -1 return value finally
	//numwords = 1;	
	//seekstatus = Seek(fileid,numwords);
	//print(seekstatus);	
	
	//this block is to write something to a file and then read it again for testing
 	if (variable == 4) then
	word = "S";
	writeStatus = Write(fileid,word);
	print(writeStatus);
	endif;
	
	if (variable == 3) then
	status = Read(fileid,word);
	print(status);
	print(word);
	endif;	

endwhile;
	return 0;
}
