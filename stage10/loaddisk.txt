load --os $HOME/myxos/stage10/os_startup.xsm
load --int=7 $HOME/myxos/stage10/int7.xsm
load --int=1 $HOME/myxos/stage10/int1.xsm
load --int=2 $HOME/myxos/stage10/int2.xsm
load --int=3 $HOME/myxos/stage10/int3.xsm
load --int=4 $HOME/myxos/stage10/int4.xsm
load --int=5 $HOME/myxos/stage10/int5.xsm
load --int=6 $HOME/myxos/stage10/int6.xsm
load --exhandler $HOME/myxos/stage10/exhandler.xsm
load --int=timer $HOME/myxos/stage10/timer.xsm


load --init $HOME/myxos/stage10/driver.xsm
load --exec $HOME/myxos/stage10/driver.xsm

